// Javascript code goes here!


let choices = ['Rock', 'Paper', 'Scissors'];
let gamesPlayed = 0
let playerScore = 0
const finalScore = 5
let computerScore = 0
let computerChoice = computerPlay();
let playerName = prompt("What's your name?")

const PlayerScoreDisplay = document.querySelector("#player-score");
const ComputerScoreDisplay = document.querySelector("#computer-score");
const PlayerPlayDisplay = document.querySelector("#player-play");
const ComputerPlayDisplay = document.querySelector("#computer-play");
const PlayerNameDisplay = document.querySelector("#player-name")
PlayerNameDisplay.textContent = playerName

const rockBtn = document.querySelector("#rock");
const paperBtn = document.querySelector("#paper");
const scissorsBtn = document.querySelector("#scissors");

rockBtn.addEventListener('click', () => {
    playRound('Rock', computerPlay())
    PlayerPlayDisplay.textContent = "👊"

});
paperBtn.addEventListener('click', () => {
    playRound('Paper', computerPlay())
    PlayerPlayDisplay.textContent = "✋"
});
scissorsBtn.addEventListener('click', () => {
    playRound('Scissors', computerPlay())
    PlayerPlayDisplay.textContent = "✌️"
});


function computerPlay() {
    let choice = choices[Math.floor(Math.random() * choices.length)];
    return choice;
};

function changeComputerEmoji(value) {
    if (value === 'Rock') {
        ComputerPlayDisplay.textContent = "👊"
    } else if (value === 'Paper') {
        ComputerPlayDisplay.textContent = "✋"
    } else if (value === 'Scissors') {
        ComputerPlayDisplay.textContent = "✌️"
    }
}

function playRound(playerValue, computerValue) {
    if (playerValue === computerValue) {
        changeComputerEmoji(computerValue)
        return console.log("DRAW!!!");
    } else if ((playerValue === 'Rock' && computerValue === 'Scissors') ||
        (playerValue === 'Paper' && computerValue === 'Rock') ||
        (playerValue === 'Scissors' && computerValue === 'Paper')
    ) {
        changeComputerEmoji(computerValue)
        playerScore++;
        PlayerScoreDisplay.textContent = `Your Score: ${playerScore}`;
        checkScore();
        return;
    } else if ((playerValue === 'Scissors' && computerValue === 'Rock') ||
        (playerValue === 'Rock' && computerValue === 'Paper') ||
        (playerValue === 'Paper' && computerValue === 'Scissors')
    ) {
        changeComputerEmoji(computerValue)
        computerScore++;
        ComputerScoreDisplay.textContent = `Computer Score: ${computerScore}`;
        checkScore();
        return;
    } else {
        return console.log('Invalid Choice! try again.');
    }
    
};

function checkScore() {
    if (playerScore === finalScore) {
        console.log("YOU WIN!!!, game restarted!");
        playerScore = 0;
        computerScore = 0;
        PlayerScoreDisplay.textContent = `Your Score: 0`;
        ComputerScoreDisplay.textContent = `Computer Score: 0`;
    } else if (computerScore === finalScore) {
        console.log("COMPUTER WINS!, game restarted");
        playerScore = 0;
        computerScore = 0;
        PlayerScoreDisplay.textContent = `Your Score: 0`;
        ComputerScoreDisplay.textContent = `Computer Score: 0`;
    };
}

